package main

import (
	"testing"
	"strconv"
	"math/big"
	"crypto/sha256"
	"bytes"
	"fmt"
	"time"
	"crypto/rand"
)

// Check that the output key-length is correct for some possible inputs of k
func TestKeyGenLength(t *testing.T) {

	testResults := []struct{ input int; resultLength int }{
		{10,0},
		{50,0},
		{100,0},
		{1000,0},
		{2000,0},
	}

	for _,row := range testResults{
		_,_, key, _ := KeyGen(row.input)
		row.resultLength = key.BitLen()

		if(row.input != row.resultLength){
			t.Error("Key length is " + strconv.Itoa(row.resultLength) + ", should be " + strconv.Itoa(row.input))
		}
	}
}

func TestEncryptDecrypt(t *testing.T) {
	keySize := []int {500, 1000, 2000} // Key sizes must be longer than the message
	zero, _ := new(big.Int).SetString("0",10)
	one, _ := new(big.Int).SetString("1",10)
	hundred, _ := new(big.Int).SetString("100",10)
	thousand, _ := new(big.Int).SetString("1000",10)
	bignumber, _ := new(big.Int).SetString("234543235343224245435",10)
	bignumber2, _ := new(big.Int).SetString("93243549549543949323921943295439594325943958932849812394839248934893284938294893284934298429348923849",10)

	testData := []struct{
		input *big.Int
		encrypted *big.Int
		decrypted *big.Int
	}{
		{hundred,zero,one},
		{thousand, zero,one},
		{bignumber,zero,one},
		{bignumber2,zero,one},
	}

	for _, key := range keySize { // Test encryption/decryption with many different key-sizes
		p,q,n,d = KeyGen(key)

		for _, row := range testData{
			encrypted := Encrypt(*row.input)
			row.encrypted = &encrypted
			decrypted := Decrypt(encrypted)
			row.decrypted = &decrypted

			if row.input.Cmp(row.decrypted) != 0 {
				t.Error("Test failed! Input/decryption mismatch: Input: ", row.input, " Decrypted data: ", row.decrypted)
			}
		}
	}
}

func TestEncryptDecryptFromFile(t *testing.T) {
	aes_key = []byte("L123HfGq5818KpS6");
	filename := "encryptedRsaKey.txt"
	p,q,n,d = KeyGen(50)
	d_clone := d

	EncryptToFile(filename);
	d = DecryptFromFile(filename);

	if d.Cmp(&d_clone) != 0 {
		t.Error("Couldnt decrypt with key from file. Original key: ", &d_clone, ", decrypted key from file: ", &d);
	}
}

func TestSigning(t *testing.T) {
	p,q,n,d = KeyGen(2000);

	message, _ := new(big.Int).SetString("123456789", 10);
	hash := sha256.Sum256(message.Bytes());
	hashSignature := Sign(*message);
	hashFromSignature := Encrypt(hashSignature);

	if bytes.Compare(hashFromSignature.Bytes(), hash[:]) != 0 {
		t.Error("Hash mismatch. Message hash: ", hash,
			", hash from signature: ", hashFromSignature.Bytes());
	}else{
		fmt.Println("TestSigning passed");
	}
}

func TestSigningModifiedMessage(t *testing.T){
	p,q,n,d = KeyGen(2000);

	message, _ := new(big.Int).SetString("123456789", 10);
	hash := sha256.Sum256(message.Bytes());
	hashSignature := signWithEvilMessageModifier(*message);
	hashFromSignature := Encrypt(hashSignature);

	if bytes.Compare(hashFromSignature.Bytes(), hash[:]) == 0 {
		t.Error("Hash match! But shouldnt. Message hash: ", hash,
			", hash from signature: ", hashFromSignature.Bytes());
	}else{
		fmt.Println("TestSigningModified passed");
	}
}

func signWithEvilMessageModifier(message big.Int) (big.Int){
	modifiedMessage, _ := new(big.Int).SetString("742321424312324", 10);
	return Sign(*modifiedMessage);
}

func TestHashingTime(t *testing.T){
	message := make([]byte, 10240); // make a 10kb message
	rand.Read(message); // Fill up with junk
	start := time.Now();
	for i := 0; i < 10000; i++{
		Sha256Hash(message);
	}
	elapsed := time.Since(start);
	fmt.Printf("Hashing 10000 messages of 10kb took: %f %s %s", elapsed.Seconds(), "seconds,","\n");
	fmt.Printf("We are hashing: %f bytes/sec \n", 102400000/elapsed.Seconds());
}

func TestRsaSignTime(t *testing.T){
	p,q,n,d = KeyGen(2000); // Generating 2000-bit RSA key
	message := make([]byte, 240); // Appr. a 2000-bit message
	rand.Read(message); // Fill up with junk
	bigint := new(big.Int).SetBytes(message);
	start := time.Now();
	Decrypt(*bigint);
	elapsed := time.Since(start);

	fmt.Printf("It took %s to sign one hash with RSA \n", elapsed.String());
}


