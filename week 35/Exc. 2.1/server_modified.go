package main

import (
	"bufio"
	"fmt"
	//"bufio"
	//"fmt"
	"net"
	"os"
	"os/signal"
	"strconv"
	"time"
)

var sentMessages float64 = 0.0
var addedClients int = 0
var start time.Time
var timeRestarted bool = false

func broadcast(connections *[]net.Conn, c chan string) {
	for {
		message := <-c
		for _, conn := range *connections {
			if conn != nil {
				fmt.Fprintf(conn, message)
				if addedClients >= 1 {
					if !timeRestarted {
						start = time.Now()
						timeRestarted = true
					}
					sentMessages++
				}
			}
		}
	}
}

func handleConnection(outbound chan string, conn net.Conn) {
	defer conn.Close()
	remoteAddr := conn.RemoteAddr().String()
	for {
		message, err := bufio.NewReader(conn).ReadString('\n')
		if err != nil {
			fmt.Println("Ending session with " + remoteAddr)
			return
		}
		reply := remoteAddr + " " + message
		outbound <- reply
	}
}

func main() {
	fmt.Println("Debug1")
	connections := make([]net.Conn, 5)
	connectionListener, _ := net.Listen("tcp", ":")
	fmt.Println("Listening on address: " + connectionListener.Addr().String())
	outbound := make(chan string)
	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, os.Interrupt)

	fmt.Println("Debug2")
	go broadcast(&connections, outbound)

	start = time.Now()
	go func() {
		<-signalChan
		fmt.Println("\nReceived an interrupt, stopping server...\n")
		connectionListener.Close()
		elapsed := time.Since(start).Seconds()
		fmt.Println("Messages sent: " + FloatToString(sentMessages))
		fmt.Println("Time elapsed: " + FloatToString(elapsed))
		messagesPerSecond := sentMessages / elapsed
		fmt.Println("Stopping server. Has sent " + FloatToString(messagesPerSecond) + " messages pr second")
	}()

	defer connectionListener.Close()
	for {

		fmt.Println("Debug4")
		conn, _ := connectionListener.Accept()

		fmt.Println("Debug5")
		fmt.Println("Connection from " + conn.RemoteAddr().String() + " accepted")
		connections = append(connections, conn)
		addedClients++
		go handleConnection(outbound, conn)
	}

}

func FloatToString(input_num float64) string {
	return strconv.FormatFloat(input_num, 'f', 2, 64)
}
