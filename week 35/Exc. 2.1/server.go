package main

import (
	"bufio"
	"fmt"
	//"bufio"
	//"fmt"
	"net"
	//"os"
)

func broadcast(connections *[]net.Conn, c chan string) {

	for {
		message := <-c
		for _, conn := range *connections {

			if conn != nil {
				fmt.Fprintf(conn, message)
			}
		}
	}
}

func handleConnection(outbound chan string, conn net.Conn) {
	defer conn.Close()
	remoteAddr := conn.RemoteAddr().String()
	for {
		message, err := bufio.NewReader(conn).ReadString('\n')
		if err != nil {
			fmt.Println("Ending session with " + remoteAddr)
			return
		}
		reply := remoteAddr + " " + message
		outbound <- reply
	}
}

func main() {
	connections := make([]net.Conn, 5)
	connectionListener, _ := net.Listen("tcp", ":")
	fmt.Println("Listening on address: " + connectionListener.Addr().String())
	outbound := make(chan string)

	go broadcast(&connections, outbound)

	defer connectionListener.Close()
	for {
		conn, _ := connectionListener.Accept()
		fmt.Println("Connection from " + conn.RemoteAddr().String() + " accepted")
		connections = append(connections, conn)
		go handleConnection(outbound, conn)
	}

}
